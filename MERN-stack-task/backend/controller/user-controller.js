const User = require("../models/user.model");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

exports.getRegUsers = async (req, res) => {
  const { name, email, password, number } = req.body;
  const image = req.file.path;

  if (!name || !email || !password || !number)
    return res.json({ message: "Invalid Credentials" });
  const userExists = await User.findOne({ name: name });

  if (userExists) return res.json({ message: "User already exists" });
  else {
    const salt = await bcrypt.genSalt();
    const hashPassword = await bcrypt.hash(password, salt);

    const newUser = new User({
      name,
      email,
      password: hashPassword,
      number,
      image,
    });
    newUser.save();

    return res.json({ message: "User Created", newUser });
  }
};

exports.getLoginUser = async (req, res) => {
  const { email, password } = req.body.data;

  if (!email || !password) return res.json({ message: "Invalid Credentials" });

  const user = await User.findOne({ email: email });
  if (user) {
    const matchPassword = await bcrypt.compare(password, user.password);
    if (matchPassword) {
      const payload = {
        email,
      };
      jwt.sign(payload, "secret", { expiresIn: "1d" }, (err, token) => {
        if (err) console.log(err);
        else {
          res.cookie("jwt", token);
          return res.json({ message: "User logged In", token: token });
        }
      });
    } else {
      return res.json({ message: "Incorrect Password" });
    }
  } else {
    return res.json({ message: "Incorreact Credentials" });
  }
};

exports.getUserList = async (req, res) => {
  return res.json({ message: "This is a protected route " });
};

exports.getUsers = async (req, res) => {
  try {
    const user = await User.find();
    res.json(user);
  } catch (error) {
    res.json({ message: error.message });
  }
};

exports.getUserById = async (req, res) => {
  try {
    const user = await User.findById(req.params.id);
    res.json(user);
  } catch (error) {
    res.json({ message: error.message });
  }
};

exports.editUser = async (req, res) => {
  const { name, email, number } = req.body;
  const image = req.file;
  // const editUser = new User(user);
  console.log(image);
  try {
    const updatedUser = await User.findOneAndUpdate(
      { _id: req.params.id },
      { name, email, number, image },
      { new: true }
    );
    res.json(updatedUser);
  } catch (error) {
    res.json({ message: error.message });
  }
};

exports.deleteUser = async (req, res) => {
  try {
    await User.deleteOne({ _id: req.params.id });
    res.json("User deleted successfully");
  } catch (error) {
    res.json({ message: error.message });
  }
};

exports.getProfile = async (req, res) => {
  try {
    const user = await User.findById(req.params.id);
    res.json(user);
  } catch (error) {
    res.json({ message: error.message });
  }
};
