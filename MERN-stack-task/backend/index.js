const bodyParser = require("body-parser");
const express = require("express");
const mongoose = require("mongoose");
const url = require("./models/db.js").url;
const cors = require("cors");
const path = require("path");
const app = express();
const PORT = 5000;


app.use(cors());
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true})); 

app.use("/uploads", express.static("uploads")); //uploads folder to save image
mongoose.set("useFindAndModify", false);
mongoose
  .connect(url, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    // useCreateIndex: true,
  })
  .then(() => {
    console.log("Databse Connected");
  })
  .catch((err) => {
    console.log(err);
  });

app.use("/users", require("./routes/route.js"));

app.listen(PORT, () => {
  console.log(`server is listening on ${PORT}`);
});
