const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const autoIncrement = require("mongoose-auto-increment");

const userModel = new Schema(
  {
    name: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      required: true,
    },
    password: {
      type: String,
      required: true,
    },
    number: {
      type: String,
      required: true,
    },
    image: {
      type: String,
      trim: true,
    }, 
  },
  {
    timestamps: true,
  }
);

autoIncrement.initialize(mongoose.connection);
userModel.plugin(autoIncrement.plugin, "User");
module.exports = mongoose.model("User", userModel);
