const express = require('express');
const route = express.Router();
const { getRegUsers, getLoginUser, getUserList, getUsers, getUserById, editUser, deleteUser, getProfile } = require('../controller/user-controller.js');

const uploadMulter = require("../middleware/upload.js");
const validation = require("../validations/validation.js");

route.post('/signup', uploadMulter, validation, getRegUsers);
route.post('/login', getLoginUser);
route.post('/userList', getUserList);
route.get('/allUsers', getUsers);
route.get("/:id", getUserById);
route.put("/:id", uploadMulter, editUser);
route.delete("/:id", deleteUser);
route.get("/view-profile/:id", getProfile);

module.exports = route; 
    