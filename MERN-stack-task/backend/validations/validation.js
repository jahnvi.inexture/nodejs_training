const fs = require("fs");
module.exports = (req, res, next) => {
  if (typeof (req.file) === "undefined") {
    return res.status(400).json({
      errors: "problem with sending data",
    });
  }
  // console.log(req.body.name)
  // let name = req.body.name
  console.log(req.file);
  const image = req.file;

  //check file type
  if (
    !req.file.mimetype.includes("jpeg") &&
    !req.file.mimetype.includes("png") &&
    !req.file.mimetype.includes("jpg")
  ) {
    fs.unlinkSync(req.file);
    return res.status(400).json({
      errors: "file not support",
    });
  }
  //check file size
  if (req.file.size > 1024 * 1024 * 2) {
    fs.unlinkSync(req.file);
    return res.status(400).json({
      errors: "File is Too large",
    });
  }
  // check if field is empty
  if (!image) {
    return res.status(400).json({
      success: false,
      message: "all fields are required",
    });
  }
  next();
};
