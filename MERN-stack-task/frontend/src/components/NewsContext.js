import { Component } from "react";
// import { Link } from "react-router-dom";
import { connect } from "react-redux";
import "./NewsContext.css";
import Loader from "./Loader";
 
class NewsContext extends Component {
  constructor(props) {
    super(props);
    this.state = {
      articles: [],
      loading: false,
    };
  }
  componentDidMount() {
    // const { fetchNewsArticles } = this.props;
    this.setState({ loading: true });
    setTimeout(() => {
      this.setState({ loading: false });
    }, 2000);

    // fetchNewsArticles();
  }
  render() {
    let data;
    if (this.state.loading) {
      data = <Loader />;
    } else {
      data = (
        <div className="all__news">
          {/* <h2>hello</h2> */}
        </div>
      );
    }
    return <div>{data}</div>;
  }
}

const mapStateToProps = (state) => ({
  articles: state.articles,
});

const mapDispatchToProps = {
  // fetchNewsArticles,
};

export default connect(mapStateToProps, mapDispatchToProps)(NewsContext);
