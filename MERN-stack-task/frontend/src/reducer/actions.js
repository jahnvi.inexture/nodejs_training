import { USER_REGISTER, LOGOUT } from "./types";
import history from "./history";
import axios from "axios";
import Cookie from "js-cookie";

const usersUrl = "http://localhost:5000/users";

export function setUserRegister(user) {
  console.log("setUserRegister");
  return {
    type: USER_REGISTER,
    user,
  };
}

export function CallUserRegister(data) {
  return async (dispatch) => {
    dispatch(setUserRegister(data));
    console.log("data", data);
    axios
      .post("http://localhost:5000/users/signup", data, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      })
      .then((res) => {
        console.log(res.data);
        setTimeout(() => {}, 1000);
      })
      .then((response) => {
        console.log(response);
        alert("You've Registered Successfully ");
        history.push("/login");
      })
      .catch((error) => {
        console.log(error.response);
        console.log("chalooooooooooo");
      });
  };
}

export async function CallUserLogin(data) {
  const res = await axios.post("http://localhost:5000/users/login", { data });
  if (res.data.token) {
    Cookie.set("jwt", res.data.token);
    history.push("/home");
    console.log(res.data.token);
  } else {
    history.push("/login");
    alert("Invalid email or password");
  }
}

export const getUsers = async (id) => {
  id = id || "";
  return await axios.get("http://localhost:5000/users/allUsers");
};

export const getUserById = async (id) => {
  return await axios.get(`${usersUrl}/${id}`);
};

export const getProfile = async (id) => {
  id = id || "";
  return await axios.get(`${usersUrl}/${id}`);
};
export const deleteUser = async (id) => {
  return await axios.delete(`${usersUrl}/${id}`);
};

export const editUser = async (id, user) => {
  return await axios.put(`${usersUrl}/${id}`, user);
};

export function UserLogout() {
  console.log("bye");
  history.push("/login");
  return {
    type: LOGOUT, 
  };
}
