import React, { useState, useEffect } from "react";
import {
  Table,
  TableHead,
  TableCell,
  TableRow,
  TableBody,
  Button,
  makeStyles,
} from "@material-ui/core";
import { getUsers, deleteUser } from "../reducer/actions";
import { Link } from "react-router-dom";
import { Suspense } from "react";
import Loader from '../components/Loader';

const useStyles = makeStyles({
  table: {
    maxWidth: "100%",
    margin: "50px 0 0 50px",
  },
  thead: {
    "& > *": {
      fontSize: 20,
      background: "#000000",
      color: "#FFFFFF",
    },
  },
  row: {
    "& > *": {
      fontSize: 18,
    },
  },
});

const AllUsers = () => {
  const [users, setUsers] = useState([]);
  const classes = useStyles();

  useEffect(() => {
    getAllUsers();
  }, []);

  const deleteUserData = async (id) => {
    await deleteUser(id);
    getAllUsers();
  };

  const getAllUsers = async () => {
    let response = await getUsers();
    setUsers(response.data);
  };

  return (
    <Table className={classes.table}>
      <TableHead>
        <TableRow className={classes.thead}>
          <TableCell>Id</TableCell>
          <TableCell>Name</TableCell>
          <TableCell>Email</TableCell>
          <TableCell>Phone</TableCell>
          <TableCell>Image</TableCell>
          <TableCell></TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {users.map((user) => (
          <TableRow className={classes.row} key={user.id}>
            <TableCell>{user._id}</TableCell>
            <TableCell>{user.name}</TableCell>
            <TableCell>{user.email}</TableCell>
            <TableCell>{user.number}</TableCell>
            <TableCell>
              <img
                src={`http://localhost:5000/${user.image}`}
                style={{ width: "90px", margintop: "20px" }}
              />
            </TableCell>
            <TableCell>
              <Button
                variant="contained"
                style={{ marginRight: 10 }}
                component={Link}
                to={`/edit/${user._id}`}
              >
                <i class="fa fa-edit" style={{ fontSize: "24px" }}></i>
              </Button>
              <Button
                variant="contained"
                style={{ width: 20 }}
                component={Link}
                onClick={() => {
                  const confirmBox = window.confirm(
                    "Do you really want to Delete " + user.name + "?"
                  );
                  if (confirmBox === true) {
                    deleteUserData(user._id);
                  }
                }}
              >
                <i class="fa fa-trash-o" style={{ fontSize: "24px" }}></i>
              </Button>
              <Button
                variant="contained"
                style={{ width: 20, marginLeft: 10 }}
                component={Link}
                to={`/view-profile/${user._id}`}
              >
                <i class="fa fa-eye" style={{ fontSize: "24px" }}></i>
              </Button>
            </TableCell>
          </TableRow>
        ))}
      </TableBody>
    </Table>
  );
};

export default AllUsers;
