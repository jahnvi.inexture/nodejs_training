import { useState, useEffect } from "react";
import {
  FormGroup,
  FormControl,
  TextField,
  Button,
  makeStyles,
  Typography,
} from "@material-ui/core";
import { useHistory, useParams } from "react-router-dom";
import { getUserById, editUser } from "../reducer/actions";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { userSchema } from "../validations/userSchema";

const initialValue = {
  name: "",
  username: "",
  email: "",
  number: "",
  image: "",
};

const useStyles = makeStyles({
  container: {
    width: "50%",
    margin: "5% 0 0 25%",
    "& > *": {
      marginTop: 20,
    },
  },
});

const EditUser = () => {
  const [user, setUser] = useState(initialValue);
  const [uploadFile, setUploadFile] = useState();
  const { name, email, number, image } = user;
  const { id } = useParams();
  const classes = useStyles();
  const history = useHistory();

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(userSchema),
    reValidateMode: "onChange",
    mode: "all",
  });

  const onSubmit = (user) => console.log(user)

  useEffect(() => {
    loadUserDetails();
  }, []);

  const loadUserDetails = async (data) => {
    const response = await getUserById(id);
    setUploadFile(response.data);
    setUser(response.data);
    console.log(response);
  };

  const editUserDetails = async () => {
    const response = await editUser(id, user);
    const formData = new FormData();
    formData.append("image", uploadFile);
    history.push("/home");
  };

  const onProfileChange = (e) => {
    console.log(e.target.files[0]);
    setUploadFile({ ...user, [e.target.Files]: e.target.files[0]})
  }

  const onValueChange = (e) => {
    console.log(e.target.value);
    setUser({ ...user, [e.target.name]: e.target.value });
  };

  return (
    <FormGroup className={classes.container} onSubmit={handleSubmit(onSubmit)}>
      <Typography variant="h4">Edit Information</Typography>
      <FormControl>
        <label>Name</label>
        <TextField
          {...register("name")}
          type="text"
          id="name"
          className="form-control"
          name="name"
          value={name}
          onChange={(e) => onValueChange(e)}
        />
        <p style={{ color: "red" }}>{errors?.name?.message}</p>
      </FormControl>
      <FormControl>
        <label>Email</label>
        <TextField
          {...register("email")}
          type="email"
          className="form-control"
          name="email"
          value={email}
          onChange={(e) => onValueChange(e)}
        />
        <p style={{ color: "red" }}>{errors?.email?.message}</p>
      </FormControl>
      <FormControl>
        <label>Phone</label>
        <TextField
          {...register("phone")}
          type="number"
          className="form-control"
          name="number"
          value={number}
          onChange={(e) => onValueChange(e)}
        />
      </FormControl>
      <FormControl>
        <label>Upload Image</label>
        <TextField
          type="file"
          className="form-control"
          name="image"
          accept="image/*"
          onChange={(e) => onProfileChange(e)}
        ></TextField>
      </FormControl>
      <FormControl>
        <Button
          variant="contained"
          color="primary"
          onClick={() => editUserDetails()}
        >
          Edit User
        </Button>
      </FormControl>
    </FormGroup>
  );
};

export default EditUser;
