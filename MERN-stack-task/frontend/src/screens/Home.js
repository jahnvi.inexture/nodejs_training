import { connect, useDispatch } from "react-redux";
import { UserLogout } from "../reducer/actions";
import AllUsers from "./AllUsers";
import NewsContext from "../components/NewsContext";
import { Button } from "@material-ui/core";
import React, { Suspense } from "react";
import Loader from "../components/Loader";

const Home = () => {
  const dispatch = useDispatch();

  const AllUsers = React.lazy(() => {
    return new Promise((resolve) => {
      setTimeout(() => resolve(import("./AllUsers")), 1500);
    });
  });

  function logout() {
    dispatch(UserLogout());
  }

  return (
    <div>
      <nav className="navbar navbar-expand-lg navbar-light">
        <div className="container">
          <Button
            // color="secondary"
            variant="contained"
            onClick={() => logout()}
          >
            Logout
          </Button>
        </div>
      </nav>
      <h2
        style={{ textAlign: "center", fontWeight: "bolder", fontSize: "40px" }}
      >
        All User Details
      </h2>
      <Suspense fallback={<Loader />}>
        <AllUsers />
      </Suspense>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    user: state,
  };
};

const mapDispatchToProps = {
  UserLogout,
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
