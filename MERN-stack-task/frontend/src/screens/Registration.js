import React, { useState } from "react";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { Link } from "react-router-dom";
import { useDispatch, connect } from "react-redux";
import { CallUserRegister } from "../reducer/actions";
import { RegistrationSchema } from "../validations/RegistrationSchema";

function Registration() {
  const [formData, setFormData] = useState();
  const [image, setImage] = useState();
  const {
    register,
    handleSubmit,
    formState: { errors },
    watch,
  } = useForm({
    resolver: yupResolver(RegistrationSchema),
    reValidateMode: "onChange",
    mode: "all",
  });

  const dispatch = useDispatch();

  const upload = (event) => {
    const file = event.target.files ? event.target.files[0] : undefined;
    console.log("file =====>", file);

    setImage(file);

    // formData.append("image", files[0]);
    // setFormData(formData);
    // console.log('setform dataaaaaa', formData)
  };

  const onSubmit = (data) => {
    var formData = new FormData();

    Object.keys(data).map((x) => {
      formData.append(x, data[x]);
    });
    formData.append("image", image);
    console.log('formData', formData);
    // const formData = new FormData();
    // formData.append("name", data["name"]);
    // formData.append("email", data["email"]);
    // formData.append("password", data["password"]);
    // formData.append("number", data["number"]);
    // formData.append("image", image);

    // console.log('formDataformData', formData)
    dispatch(CallUserRegister(formData));
  };
  console.log("all fields ========>", watch());
  return (
    <>
      <div className="outer">
        <div className="inner">
          <form onSubmit={handleSubmit(onSubmit)} enctype="multipart/form-data">
            <h3>Register</h3>
            <div className="form-group">
              <label id="r1">Name</label>
              <input
                type="text"
                className="form-control"
                placeholder="Enter Full Name"
                name="name"
                id="R1"
                {...register("name")}
              />
              <p class="styles">{errors.name?.message}</p>
            </div>

            <div className="form-group">
              <label id="r2">Email</label>
              <input
                type="email"
                className="form-control"
                placeholder="Enter email"
                id="R2"
                name="email"
                {...register("email")}
              />
              <p class="styles">{errors.email?.message}</p>
            </div>

            <div className="form-group">
              <label id="r3">Password</label>
              <input
                type="password"
                className="form-control"
                placeholder="Enter password"
                id="R3"
                name="password"
                {...register("password")}
              />
              <p class="styles">{errors.password?.message}</p>
            </div>

            <div className="form-group">
              <label id="r4">Phone No.</label>
              <input
                type="number"
                className="form-control"
                placeholder="Enter contact no"
                id="R4"
                name="number"
                {...register("number")}
              />
              <p class="styles">{errors.number?.message}</p>
            </div>

            <div className="form-group">
              <label id="r4">Upload Image</label>
              <input
                type="file"
                className="form-control"
                id="R4"
                name="image"
                onChange={upload}
              />
            </div>

            <button
              type="submit"
              className="btn btn-dark btn-lg btn-block"
              id="regbtn"
            >
              Register
            </button>
            <p className="forgot-password text-right">
              Already registered <Link to="/login">Login?</Link>
            </p>
          </form>
        </div>
      </div>
    </>
  );
}
const mapStateToProps = (state) => {
  return {
    user: state,
  };
};

const mapDispatchToProps = {
  CallUserRegister,
};

export default connect(mapStateToProps, mapDispatchToProps)(Registration);
