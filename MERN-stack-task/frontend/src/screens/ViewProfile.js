// import "./ViewProfile.css";
import { Button } from "@material-ui/core";
import { useHistory, useParams } from "react-router-dom";
import React, { useState, useEffect } from "react";
import { getProfile } from "../reducer/actions";

function ViewProfile() {
  const [users, setUsers] = useState([]);
  const history = useHistory();
  const { id } = useParams();

  useEffect(() => {
    loadUserDetails();
  }, []);

  const loadUserDetails = async () => {
    const response = await getProfile(id);
    setUsers(response.data);
  };

  const handleProfile = async () => {
    history.push("/home");
  };
  return (
    <div>
      {/* {users.user._id((user) => { */}
      <div className="card" style={{ marginTop: "30vh" }}>
        <img
          src={`http://localhost:5000/${users.image}`}
          style={{ width: "359px" }}
        />
        <p>Name:{users.name}</p>
        <p>Email:{users.email}</p>
        <p>Phone:{users.number}</p>
        <div style={{ margin: "24px" }}></div>
        <p>
          <Button
            color="secondary"
            variant="contained"
            onClick={() => handleProfile()}
          >
            back
          </Button>
        </p>
      </div>
      {/* })} */}
    </div>
  );
}

export default ViewProfile;
