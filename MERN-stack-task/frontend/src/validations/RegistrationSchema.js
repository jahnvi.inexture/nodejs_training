import * as yup from "yup";

export const RegistrationSchema = yup.object().shape({
    name: yup.string("Name must be characters only").required("*Required"),
    email: yup
      .string()
      .email("Please enter a valid email address")
      .required("*Required"),
    password: yup.string().min(6).max(10).required("*Required"),
    number: yup
      .number()
      .typeError("Phone No. must be a number")
      .integer("A phone number can't include a decimal point")
      .min(10, "Phone length should be 10"),
  });

