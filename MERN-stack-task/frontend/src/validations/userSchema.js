import * as yup from "yup";

export const userSchema = yup.object().shape({
  name: yup.string().required("Name is required").min(5),
  email: yup.string().required("Please enter valid Email").email(),
  phone: yup.string().required("Phone Number is required"),
});
