import { useState } from "react";
import { addUser } from "../Service/api";
import { useHistory } from "react-router-dom";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { userSchema } from "../validations/userSchema";
import {
  FormGroup,
  FormControl,
  TextField,
  Button,
  makeStyles,
  Typography,
} from "@material-ui/core";

const initialValue = {
  name: "",
  username: "",
  email: "",
  password: "",
  phone: "",
};

const useStyles = makeStyles({
  container: {
    width: "50%",
    margin: "5% 0 0 25%",
    "& > *": {
      marginTop: 20,
    },
  },
});

const AddUser = () => {
  const [user, setUser] = useState(initialValue);
  const { name, username, email, password, phone } = user;
  const history = useHistory();
  const classes = useStyles();

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(userSchema),
    reValidateMode: "onChange",
    mode: "all",
  });
  const onSubmit = (user) => console.log(user);

  const onValueChange = (e) => {
    console.log(e.target.value);
    setUser({ ...user, [e.target.name]: e.target.value });
  };

  const addUserDetails = async () => {
    await addUser(user);
    history.push("./all");
  };

  return (
    <div className="outer">
      <FormGroup
        className={classes.container}
        onSubmit={handleSubmit(onSubmit)}
      >
        <Typography variant="h4">Add User</Typography>
        <FormControl>
          <label>Name</label>
          <TextField
            {...register("name")}
            type="text"
            id="name"
            className="form-control"
            name="name"
            value={name}
            onChange={(e) => onValueChange(e)}
          />
          <p style={{ color: "red" }}>{errors?.name?.message}</p>
        </FormControl>
        <FormControl>
          <label>Username</label>
          <TextField
            {...register("username")}
            type="text"
            id="username"
            className="form-control"
            name="username"
            value={username}
            onChange={(e) => onValueChange(e)}
          />
          <p style={{ color: "red" }}>{errors?.username?.message}</p>
        </FormControl>

        <FormControl>
          <label>Email</label>
          <TextField
            {...register("email")}
            type="email"
            className="form-control"
            name="email"
            value={email}
            onChange={(e) => onValueChange(e)}
          />
          <p style={{ color: "red" }}>{errors?.email?.message}</p>
        </FormControl>

        <FormControl>
          <label>Password</label>
          <TextField
            {...register("password")}
            type="password"
            className="form-control"
            name="password"
            value={password}
            onChange={(e) => onValueChange(e)}
          />
          <p style={{ color: "red" }}>{errors?.password?.message}</p>
        </FormControl>
        <FormControl>
          <label>Phone</label>
          <TextField
            {...register("phone")}
            type="number"
            className="form-control"
            name="phone"
            value={phone}
            onChange={(e) => onValueChange(e)}
          />
          <p style={{ color: "red" }}>{errors?.phone?.message}</p>
        </FormControl>

        <Button
          variant="contained"
          color="primary"
          onClick={() => addUserDetails()}
        >
          Add User
        </Button>
      </FormGroup>
    </div>
  );
};

export default AddUser;
