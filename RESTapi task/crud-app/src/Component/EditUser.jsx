import { useState, useEffect } from "react";
import {
  FormGroup,
  FormControl,
  TextField,
  Button,
  makeStyles,
  Typography,
} from "@material-ui/core";
import { useHistory, useParams } from "react-router-dom";
import { getUsers, editUser } from "../Service/api";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { userSchema } from "../validations/userSchema";

const initialValue = {
  name: "",
  username: "",
  email: "",
  password: "",
  phone: "",
};

const useStyles = makeStyles({
  container: {
    width: "50%",
    margin: "5% 0 0 25%",
    "& > *": {
      marginTop: 20,
    },
  },
});

const EditUser = () => {
  const [user, setUser] = useState(initialValue);
  const { name, username, email, password, phone } = user;
  const { id } = useParams();
  const classes = useStyles();
  const history = useHistory();

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(userSchema),
    reValidateMode: "onChange",
    mode: "all",
  });
  const onSubmit = (user) => console.log(user);

  useEffect(() => {
    loadUserDetails();
  }, []);

  const loadUserDetails = async () => {
    const response = await getUsers(id);
    setUser(response.data);
  };

  const editUserDetails = async () => {
    const response = await editUser(id, user);
    history.push("/all");
  };

  const onValueChange = (e) => {
    console.log(e.target.value);
    setUser({ ...user, [e.target.name]: e.target.value });
  };

  return (
    <FormGroup className={classes.container} onSubmit={handleSubmit(onSubmit)}>
      <Typography variant="h4">Edit Information</Typography>
      <FormControl>
        <label>Name</label>
        <TextField
          {...register("name")}
          type="text"
          id="name"
          className="form-control"
          name="name"
          value={name}
          onChange={(e) => onValueChange(e)}
        />
        <p style={{ color: "red" }}>{errors?.name?.message}</p>
      </FormControl>
      <FormControl>
        <label>Username</label>
        <TextField
          {...register("username")}
          type="text"
          id="username"
          className="form-control"
          name="username"
          value={username}
          onChange={(e) => onValueChange(e)}
        />
        <p style={{ color: "red" }}>{errors?.username?.message}</p>
      </FormControl>
      <FormControl>
        <label>Email</label>
        <TextField
          {...register("email")}
          type="email"
          className="form-control"
          name="email"
          value={email}
          onChange={(e) => onValueChange(e)}
        />
        <p style={{ color: "red" }}>{errors?.email?.message}</p>
      </FormControl>
      <FormControl>
        <label>Password</label>
        <TextField
          {...register("password")}
          type="password"
          className="form-control"
          name="password"
          value={password}
          onChange={(e) => onValueChange(e)}
        />
        <p style={{ color: "red" }}>{errors?.password?.message}</p>
      </FormControl>
      <FormControl>
      <label>Phone</label>
        <TextField
         {...register("phone")}
         type="number"
         className="form-control"
         name="phone"
         value={phone}
         onChange={(e) => onValueChange(e)}
        />
      </FormControl>
      <FormControl>
        <Button
          variant="contained"
          color="primary"
          onClick={() => editUserDetails()}
        >
          Edit User
        </Button>
      </FormControl>
    </FormGroup>
  );
};

export default EditUser;
