import express from "express";
import mongoose from "mongoose";
import cors from "cors";
import bodyParser from "body-parser";

import route from "./route/routes.js";

const app = express();

// app.use(cors({ origin: 'http://localhost:3000' }));
app.use(cors());
app.use(bodyParser.json({ extended: true }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use("/users", route);

const PORT = 5000;
const URL =
  "mongodb://user:crudtask@cluster0-shard-00-00.kymqx.mongodb.net:27017,cluster0-shard-00-01.kymqx.mongodb.net:27017,cluster0-shard-00-02.kymqx.mongodb.net:27017/CRUDAPP?ssl=true&replicaSet=atlas-1llw2p-shard-0&authSource=admin&retryWrites=true&w=majority";

mongoose
  .connect(URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
  })
  .then(() => {
    app.listen(PORT, () => {
      console.log(`server is listening on ${PORT}`);
    });
  })
  .catch((error) => {
    console.log("Error: ", error.message);
  });
