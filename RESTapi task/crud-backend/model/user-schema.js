import mongoose from "mongoose";
import autoIncrement from "mongoose-auto-increment";

const userSchema = mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  username: {
    type: String,
    required: 'Username is required'
  },
  email: {
    type: String,
    required: 'email is required'
  },
  password: String,
  phone: Number,
});

autoIncrement.initialize(mongoose.connection);
userSchema.plugin(autoIncrement.plugin, "user");
const user = mongoose.model("user", userSchema);

export default user;
