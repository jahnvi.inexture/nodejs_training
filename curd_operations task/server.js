require("./models/db");

const express = require("express");
const Handlebars = require('handlebars');
const {allowInsecurePrototypeAccess} = require('@handlebars/allow-prototype-access');
const path = require("path");
const exphbs = require("express-handlebars");
const bodyparser = require("body-parser");

const userController = require("./controllers/userController");

var app = express();
app.use(
  express.urlencoded({
    extended: true,
  })
);
app.use(express.json())

app.set("views", path.join(__dirname, "/views/"));
// app.engine(
//   "hbs",
//   exphbs({
//     extname: "hbs",
//     defaultLayout: "mainLayout",
//     layoutsDir: __dirname + "/views/layouts/",
//   })
// );
// app.set("view engine", "hbs");

app.engine('handlebars', exphbs({
  defaultLayout: 'mainLayout', 
  layoutsDir: __dirname + '/views/layouts/',
  handlebars: allowInsecurePrototypeAccess(Handlebars) }));
  app.set('view engine', 'handlebars');

app.listen(3000, () => {
  console.log("server is listening on port : 3000");
});

app.use("/user", userController);
