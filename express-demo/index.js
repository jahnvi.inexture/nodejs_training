import Express from "express";
import Products from "./products.js";

const app = Express();
const port = 3000;
// const port = process.env.PORT || 3000;
app.use(Express.json());
app.use(Express.urlencoded({ extended: true }));

//Create custom middleware 
function mid(req, res, next) {
    console.log(req.query);
    console.log(req.params);
    next();
}

//GET - how you get information   app.get()
//PUT - how you chnage information    app.put()
//POST - how you post, add information   app.post()
//DELETE  - how you delete information    app.delete()

//----------------Start with GET-----------------

//To get all the products
// app.get('/', (req, res) => {
//     // res.send('Hello World')
//     res.json(Products)
// })

//To get single product
app.get("/products/:id", mid, (req, res) => {
  res.json(
    Products.find((product) => {
      return +req.params.id === product.id;
    })
  );
  //   res.send(req.params.id);
  //   res.json(Products);
});

//-----------------Start with post----------------

app.post("/add", (req, res) => {
  res.send(req.body); //this body is whats comes over in the response
});


app.delete('/products/:id', (req, res) => {
  
})








app.listen(port, () => console.log("listening on port " + port));

