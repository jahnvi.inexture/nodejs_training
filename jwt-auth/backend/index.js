const bodyParser = require("body-parser");
const express = require("express");
const mongoose = require("mongoose");
const url = require("./models/db.js").url;
const cors = require('cors');
const app = express();
const PORT = 5000;

app.use(cors());
app.use(bodyParser.json());
mongoose.set("useFindAndModify", false);
mongoose
  .connect(url, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    console.log("Databse Connected");
  })
  .catch((err) => {
    console.log(err);
  }); 

app.use("/auth", require("./router/routes.js"));

app.listen(PORT, () => {
  console.log(`server is listening on ${PORT}`);
});
