const yargs = require("yargs");
const { describe, conflicts, argv } = require("yargs");
const notes = require('./notes.js')

// Cutomize yargs version
yargs.version("1.1.0");

// Create add command
yargs.command({
  command: "add",
  describe: "Add a new note!",
  builder: {
    title: {
      describe: "Note title",
      demandOption: true,
      type: 'string'
    },
    body: {
        describe: 'Note description',
        demandOption: true,
        type: 'string'
    }
  },
  handler: function (argv) {
    notes.addNote(argv.title, argv.body)
    // console.log('Title: ' + argv.title);
    // console.log('Body: ' + argv.body);
  },
});

// Create remove command
yargs.command({
  command: "remove",
  describe: "Remove a new note!",
  builder: {
    title: {
      describe: 'Note title',
      demandOption: true,
      type: 'string'
    }
  },
  handler: function (argv) {
    notes.removeNote(argv.title)
  },
});

// Create list command
yargs.command({
  command: "list",
  describe: "Listing your notes",
  handler: function () {
    console.log("listing out all notes");
  },
});

// Create read command
yargs.command({
  command: "read",
  describe: "Read a note",
  handler: function () {
    console.log("Reading a note");
  },
});

yargs.parse()

