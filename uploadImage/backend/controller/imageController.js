const Image = require("../models/image.model.js");

exports.createImage = (req, res) => {
  const name = req.body.name;
  const image = req.file.path;

  const newImage = new Image({
    name: name,
    image: image,
  });

  newImage.save((err, newImage) => {
    if (err) {
      return res.status(400).json({
        errors: err.message,
      });
    }

    return res.json({
        message: 'Image uploaded successfully',
        newImage
    })
  });
};
