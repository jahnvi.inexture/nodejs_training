const multer = require("multer");
const path = require("path");

const storage = multer.diskStorage({
  //destinations to uploads folder
  destination: function (req, res, cb) {
    cb(null, "./uploads");
  },
  filename: function (req, file, cb) {
    console.log(file);
    //generate unique name for each image
    cb(null, "Jahnvi" + "-" + Date.now() + path.extname(file.originalname));
  },
});

// file filter
const fileFilter = (req, file, cb) => {
    cb(null, true)
}

const upload = multer({
    storage: storage,
    fileFilter: fileFilter
})

module.exports = upload.single('categoryImage');
