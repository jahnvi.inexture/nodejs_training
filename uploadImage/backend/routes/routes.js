const express = require("express");
const router = express.Router();

//multer
const uploadMulter = require("../middleware/upload.js");

//validation
const validation = require("../validations/validation.js");

//controller
const { createImage } = require("../controller/imageController.js");

router.post("/uploadImage", uploadMulter, validation, createImage);

module.exports = router;
