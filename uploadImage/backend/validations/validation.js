const fs = require("fs");

module.exports = (req, res, next) => {
  if (typeof req.file === "undefined" || typeof req.body === "undefined") {
    //if error
    return res.status(400).json({
      errors: "There is some problem while sending a data",
    });
  }
  //get image and name
  console.log(req.file);
  const name = req.body.name;
  const image = req.file.path;

  //checking the type of image
  if (
    !req.file.mimetype.includes("jpeg") &&
    !req.file.mimetype.includes("png") &&
    !req.file.mimetype.includes("jpg")
  ) {
    //remove file
    fs.unlinkSync(image);
    return res.status(400).json({
      errors: "This file format is not supported. Use Images only.",
    });
  }

  //check file size
  if (req.file.size > 1024 * 1024 * 2) {
    //remove file
    fs.unlinkSync(image);
    return res.status(400).json({
      errors: "File is too large",
    });
  }

  //check if field is empty
  if (!name || !image) {
    return res.status(400).json({
      errors: "All fields are required",
    });
  }
  next();
};
