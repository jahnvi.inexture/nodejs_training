import React, { useState } from "react";
import axios from "axios";

function App() {
  const [formData, setFormData] = useState();
  const [info, setInfo] = useState({
    name: "",
    image: "",
  });
  const [progressbar, setProgressbar] = useState(0);
  const [error, setError] = useState({
    found: false,
    message: "",
  });

  //upload Image
  const upload = ({ target: { files } }) => {
    const data = new FormData();
    data.append("categoryImage", files[0]);
    data.append("name", files.name);
    setFormData(data);
  };

  //send request
  const handleSubmit = (e) => {
    e.preventDefault();
    setInfo({
      image: "",
      name: "",
    });

    setProgressbar(0);
    const options = {
      onUploadProgress: (progressEvent) => {
        const { loaded, total } = progressEvent;
        const percent = Math.floor((loaded * 100) / total);
        console.log(`${loaded}kb of ${total}kb | ${percent}%`);
        setProgressbar(percent);
      },
    };

    axios
      .post("http://localhost:8000/api/uploadImage", formData, options)
      .then((res) => {
        console.log(res.data);
        setTimeout(() => {
          setInfo(res.data.newImage);
          setProgressbar(0);
        }, 1000);
      })
      .catch((err) => {
        console.log(err.response);
        setError({
          found: true,
          message: err.response.data.errors,
        });
        setTimeout(() => {
          setError({
            found: false,
            message: '',
          });
          setProgressbar(0);
        }, 3000);
      });
  };

  return (
    <div
      style={{ width: "100vw", height: "100vh" }}
      className="d-flex justify-content-center align-items-center flex-column"
    >
      {error.found && (
        <div
          className="alert alert-danger"
          role="alert"
          style={{ width: "359px" }}
        >
          {error.message}
        </div>
      )}

      <form onSubmit={handleSubmit} style={{ width: "359px" }}>
        <div className="progress mb-3 w-100">
          <div
            className="progress-bar"
            role="progressbar"
            style={{ width: `${progressbar}%` }}
            aria-valuenow={progressbar}
            aria-valuemin={0}
            aria-valuemax={100}
          >
            {progressbar}
          </div>
        </div>
        <div className="custom-file mb-3">
          <input
            type="file"
            className="custom-file-input"
            id="inputGroupFile04"
            aria-describedby="inputGroupFileAddon04"
            onChange={upload}
          />
          <label className="custom-file-label" htmlFor="inputGroupFile04">
            Choose file
          </label>
        </div>
        <button type="submit" className="btn btn-primary w-100">
          Submit
        </button>
      </form>
      <img
        className="mt-3"
        src={`http://localhost:8000/${info.image}`}
        alt={`${info.name}`}
        style={{ width: "359px" }}
      />
    </div>
  );
}

export default App;
